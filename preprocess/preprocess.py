import os
from cv2 import cv2
import numpy as np
from tqdm import tqdm


DIR_IMG = "cropped_images/images/"
DIR_MASK = "cropped_images/annotated_images/annotations/"


WIDTH = 200
HEIGHT = 125

def read_data():
    """
    return all images/masks in array
    """
    _x = []
    _y = []
    num_classes = 1
    folders = ['../dataset/cat1/', '../dataset/cat2/', '../dataset/cat3/',
               '../dataset/cat4/', '../dataset/cat5/', '../dataset/cat6/',
               '../dataset/cat7/', '../dataset/cat8/', '../dataset/cat9/',
               '../dataset/cat10/']
    #folders = ['../dataset/cat1/']
    for folder in tqdm(folders, 'Prepare data'):
        for image in os.listdir(folder+DIR_IMG):
            img = cv2.imread(folder+DIR_IMG+image)
            img = cv2.resize(img, (WIDTH, HEIGHT))/255
            _x.append(img)

        for mask in os.listdir(folder+DIR_MASK):
            mask_seg = cv2.imread(folder+DIR_MASK+mask)
            mask_seg = cv2.resize(mask_seg, (WIDTH, HEIGHT))[:, :, 1]
            #TODO for mask uncomment these lines
            #mask_result = np.zeros(shape=(HEIGHT, WIDTH, num_classes), dtype=np.float32)
            #mask_result[np.where(mask_seg == 1)[0], np.where(mask_seg == 1)[1]] = 1
            #print(mask_result.shape)
            #cv2.imshow("qq",mask_result)
            #cv2.waitKey(0)
            #exit()
            #_y.append(mask_result)
            _y.append(mask_seg)

    images = np.array(_x)
    masks = np.array(_y)
    masks = masks.reshape(masks.shape[0], masks.shape[1], masks.shape[2], 1)
    return images, masks, num_classes
