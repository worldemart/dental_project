#!/usr/bin python3
import sys
sys.path.append('../preprocess/')
import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import preprocess as pp
from termcolor import colored
from sklearn.model_selection import train_test_split


EPOCHS = 5000
BATCH_SIZE = 100

def crop(tensor1, tensor2):
    """
    return tensor cropped
    """
    offsets = (0,
               (int(tensor1.get_shape()[1]) - int(tensor2.get_shape()[1]))//2,
               (int(tensor1.get_shape()[2]) - int(tensor2.get_shape()[2]))//2,
               0)
    size = (-1, int(tensor2.get_shape()[1]), int(tensor2.get_shape()[2]), -1)
    return tf.slice(tensor1, offsets, size)


def max_pool(conv, pool_size):
    """
    return the pool
    """
    pool = tf.nn.max_pool(conv,
                          ksize=[1, pool_size, pool_size, 1],
                          strides=[1, pool_size, pool_size, 1],
                          padding='SAME')
    return pool


def conv2d(convul, kernel, stride, out, padding, norm):
    """
    return conv
    """
    shape = (kernel, kernel, convul.get_shape()[-1], out)
    print("shape :", shape)
    conv_w = tf.Variable(tf.random.truncated_normal(shape=shape, dtype=tf.float32))
    conv_b = tf.Variable(tf.zeros(out))
    conv = tf.nn.conv2d(convul, conv_w, strides=[1, stride, stride, 1], padding=padding) + conv_b
    conv = tf.nn.bias_add(conv, conv_b)
    if norm is True:
        conv = tf.compat.v1.layers.batch_normalization(conv, training=norm) #TODO change for native tf v2
        conv = tf.nn.relu(conv)
    return conv


def conv2d_t(convul, kernel, stride, out, padding, norm):
    """
    return deconvolution
    """
    shape = (kernel, kernel, out, convul.get_shape()[-1])
    print("shape_T :", shape)
    conv_w = tf.Variable(tf.random.truncated_normal(shape=shape, dtype=tf.float32))
    conv_b = tf.Variable(tf.zeros(out))
    if padding == 'VALID':
        out_h = (convul.get_shape()[1]-1)*stride+kernel
        out_w = (convul.get_shape()[2]-1)*stride+kernel
    elif padding == 'SAME':
        out_h = (convul.get_shape()[1]-1)*stride+1
        out_w = (convul.get_shape()[2]-1)*stride+1
    else:
        print("err padding param name")
        exit()
    b_size = tf.shape(x)[0]
    conv_t = tf.nn.conv2d_transpose(convul,
                                    conv_w,
                                    output_shape=[b_size, out_h, out_w, out],
                                    strides=[1, stride, stride, 1],
                                    padding=padding)
    conv_t = tf.nn.bias_add(conv_t, conv_b)
    if norm is True:
        conv_t = tf.compat.v1.layers.batch_normalization(conv_t, training=norm) #TODO change for native tf v2
        conv_t = tf.nn.relu(conv_t)
    return conv_t


def unet(x, nbr_mask, size, padding):
    """
    param x        : the tensor input
    param nbr_mask : number of classes
    param size     : picture's size
    param paddinf  : padding selection
    
    return output and mask
    """
    conv1 = conv2d(x, 3, 1, 32, padding, True)
    conv1 = conv2d(conv1, 3, 1, 32, padding, True)
    pool1 = max_pool(conv1, 2)

    conv2 = conv2d(pool1, 3, 1, 64, padding, True)
    conv2 = conv2d(conv2, 3, 1, 64, padding, True)
    pool2 = max_pool(conv2, 2)

    conv3 = conv2d(pool2, 3, 1, 128, padding, True)
    conv3 = conv2d(conv3, 3, 1, 128, padding, True)
    pool3 = max_pool(conv3, 2)

    conv4 = conv2d(pool3, 3, 1, 256, padding, True)
    conv5 = conv2d(conv4, 3, 1, 256, padding, True)

    deconv3 = conv2d_t(conv5, 3, 2, 256, padding, True)
    conv3 = crop(conv3, deconv3)
    concat1 = tf.concat((deconv3, conv3), axis=3)

    conv6 = conv2d(concat1, 3, 1, 128, padding, True)
    conv7 = conv2d(conv6, 3, 1, 128, padding, True)

    deconv2 = conv2d_t(conv7, 3, 2, 128, padding, True)
    conv2 = crop(conv2, deconv2)
    concat2 = tf.concat((deconv2, conv2), axis=3)

    conv8 = conv2d(concat2, 3, 1, 64, padding, True)
    conv9 = conv2d(conv8, 3, 1, 64, padding, True)

    deconv1 = conv2d_t(conv9, 3, 2, 64, padding, True)
    conv1 = crop(conv1, deconv1)
    concat3 = tf.concat((deconv1, conv1), axis=3)

    conv10 = conv2d(concat3, 3, 1, 32, padding, True)
    conv11 = conv2d(conv10, 3, 1, 32, padding, True)

    output = conv2d(conv11, 1, 1, nbr_mask, padding, False)
    output = tf.compat.v1.image.resize_images(output, (x.get_shape()[1], x.get_shape()[2]))
    mask = tf.nn.sigmoid(output, name='output')
    return output, mask

X, Y , num_classes = pp.read_data()
print(X.shape)
print(Y.shape)

x = tf.keras.backend.placeholder(shape=(None, X.shape[1], X.shape[2], 3), dtype=tf.float32, name='input')
y = tf.keras.backend.placeholder(shape=(None, Y.shape[1], Y.shape[2], num_classes), dtype=tf.float32)
print(x.shape)
print(y.shape)


output, mask = unet(x, num_classes, (X.shape[1], Y.shape[2], 3), 'VALID')
"""
with tf.Session() as sess:
    writer = tf.summary.FileWriter('logs', sess.graph)
    print(sess.run(output))
    writer.close()
"""

l_rate = 0.01
#cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=y, logits = output)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=output)

loss_operation = tf.reduce_mean(cross_entropy)
optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate = l_rate)
training_operation = optimizer.minimize(loss_operation)
correct_prediction = tf.equal(tf.round(mask), y)
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

saver = tf.train.Saver()


X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=.2)


sess = tf.Session()
with sess as sess:
    sess.run(tf.global_variables_initializer())
    num_examples = len(X_train)
    print("Training...")
    for i in range(EPOCHS):
        for offset in range(0, num_examples, BATCH_SIZE):
            end = offset + BATCH_SIZE
            batch_x, batch_y = X_train[offset:end], y_train[offset:end]
            sess.run(training_operation, feed_dict={x: batch_x, y: batch_y})

